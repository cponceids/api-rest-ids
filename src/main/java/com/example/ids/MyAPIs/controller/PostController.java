package com.example.ids.MyAPIs.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.ids.MyAPIs.entities.Post;
import com.example.ids.MyAPIs.services.PostService;


@RestController
@CrossOrigin
public class PostController {
	@Autowired
	private PostService service;
	
	@GetMapping("/api/blog")
	public List<Post> contultaPost(){ 
		List<Post> post = service.consultaPost();
		System.out.println("Blog: " + post);
		return post;
	}
	
	@GetMapping("/api/blog/{postId}")
	public Post contultaUser(@PathVariable(name="postId") Long postId){ 
		Post post = service.consultaPost(postId);
		System.out.println("Post encontrado " + post);
		return post;
	}
	
	@PostMapping("/api/blog")
	public void guardarPost(@RequestBody Post post){ 
		service.guardarPost(post);
		System.out.println("Post Guardado exitosamente..." + post);
	}
	
	@DeleteMapping("/api/blog/{postId}")
	public void borrarPost(@PathVariable(name="postId") Long postId){ 
		service.borrarPost(postId);
		System.out.println("Post Eliminado Satisfactoriamente..." + postId);
	}
	
	@PutMapping("/api/blog/{postId}")
	public void actualizarPost(@RequestBody Post post,@PathVariable(name="postId") Long postId){ 
		service.actualizarPost(post);
		System.out.println("Post Actualizado Exitosamente..." + post);
	}
}
