package com.example.ids.MyAPIs.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.ids.MyAPIs.entities.User;
import com.example.ids.MyAPIs.services.UserService;

@RestController
@CrossOrigin
public class UserController {
	@Autowired
	private UserService service;
	
	@GetMapping("/api/users")
	public List<User> contultaUser(){ 
		List<User> users = service.consultaUsers();
		System.out.println("Lista Usuarios: " + users);
		return users;
	}
	
	@GetMapping("/api/users/{userId}")
	public User contultaUser(@PathVariable(name="userId") Long userId){ 
		User user = service.consultaUser(userId);
		System.out.println("Usuario encontrado " + user);
		return user;
	}
	
	@PostMapping("/api/users")
	public void guardarUser(@RequestBody User user){ 
		service.guardarUser(user);
		System.out.println("Usuario Guardado exitosamente...");
	}
	
	@DeleteMapping("/api/users/{userId}")
	public void borrarUser(@PathVariable(name="userId") Long userId){ 
		service.borrarUser(userId);
		System.out.println("Usuario Eliminado Satisfactoriamente..." + userId);
	}
	
	@PutMapping("/api/users/{userId}")
	public void actualizarUser(@RequestBody User user,@PathVariable(name="userId") Long userId){ 
		service.actualizarUser(user);
		System.out.println("User Actualizado Exitosamente...");
	}
	
}
