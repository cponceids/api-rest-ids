package com.example.ids.MyAPIs.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.ids.MyAPIs.entities.Alumno;
import com.example.ids.MyAPIs.services.AlumnoService;

@RestController
@CrossOrigin
public class AlumnoRestController {
	
	@Autowired
	private AlumnoService service;
	
	@GetMapping("/api/alumnos")
	public List<Alumno> contultaAlumno(){ 
		List<Alumno> alumnos = service.consultaAlumno();
		System.out.println("Lista Alumnos: " + alumnos);
		return alumnos;
	}
	
	@GetMapping("/api/alumnos/{alumnoId}")
	public Alumno contultaAlumno(@PathVariable(name="alumnoId") Long alumnoId){ 
		Alumno alumno = service.consultaAlumno(alumnoId);
		System.out.println("Alumno encontrado " + alumno);
		return alumno;
	}
	
	@PostMapping("/api/alumnos")
	public void guardarAlumno(@RequestBody Alumno alumno){ 
		service.guardarAlumno(alumno);
		System.out.println("Alumno Guardado exitosamente...");
	}
	
	@DeleteMapping("/api/alumnos/{alumnoId}")
	public void borrarAlumno(@PathVariable(name="alumnoId") Long alumnoId){ 
		service.borrarAlumno(alumnoId);
		System.out.println("Alumno Eliminado Satisfactoriamente..." + alumnoId);
	}
	
	@PutMapping("/api/alumnos/{alumnoId}")
	public void actualizarAlumno(@RequestBody Alumno alumno,@PathVariable(name="alumnoId") Long alumnoId){ 
		service.actualizarAlumno(alumno);
		System.out.println("Alumno Actualizado Exitosamente...");
	}
	
}
