package com.example.ids.MyAPIs.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.example.ids.MyAPIs.entities.Contacto;
import com.example.ids.MyAPIs.services.ContactoService;

@RestController
@CrossOrigin
public class ContactoRestContoller {
	
	@Autowired
	private ContactoService service;
	
	@GetMapping("/api/contactos")
	public List<Contacto> contultaContacto(){ 
		List<Contacto> contactos = service.consultaContacto();
		System.out.println("Lista Contactos: " + contactos);
		return contactos;
	}
	
	@GetMapping("/api/contactos/{contactosId}")
	public Contacto contultaContactos(@PathVariable(name="contactoId") Long contactoId){ 
		Contacto contacto= service.consultaContacto(contactoId);
		System.out.println("Contacto encontrado " + contacto);
		return contacto;
	}
	
	@PostMapping("/api/contactos")
	public void guardarContacto(@RequestBody Contacto contacto){ 
		service.guardarContacto(contacto);
		System.out.println("Contacto Guardado exitosamente..." + contacto);
	}
	
	@DeleteMapping("/api/contactos/{contactoId}")
	public void borrarContacto(@PathVariable(name="contactoId") Long contactoId){ 
		service.borrarContacto(contactoId);
		System.out.println("Contacto Eliminado Satisfactoriamente..." + contactoId);
	}
	
	@PutMapping("/api/contacto/{contactoId}")
	public void actualizarContacto(@RequestBody Contacto contacto,@PathVariable(name="contactoId") Long contactoId){
		service.actualizarContacto(contacto);
		System.out.println("Contacto Actualizado Exitosamente...");
	}
	
}
