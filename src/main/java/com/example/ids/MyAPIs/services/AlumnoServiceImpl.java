package com.example.ids.MyAPIs.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.ids.MyAPIs.entities.Alumno;
import com.example.ids.MyAPIs.repositories.AlumnoRepository;

@Service
public class AlumnoServiceImpl implements AlumnoService{
	
	@Autowired
	private AlumnoRepository repository;
	@Override
	public List<Alumno> consultaAlumno() {
		return repository.findAll();
	}
	@Override
	public Alumno consultaAlumno(long alumnoId) {
		Optional<Alumno> alumno = repository.findById(alumnoId);
		if(alumno.isPresent()) {
			return alumno.get();
		}
		return new Alumno();
	}
	@Override
	public void guardarAlumno(Alumno alumno) {
		repository.save(alumno);
	}
	@Override
	public void borrarAlumno(long alumnoId) {
		repository.deleteById(alumnoId);
	}
	@Override
	public void actualizarAlumno(Alumno alumno) {
		repository.save(alumno);
	}
}
