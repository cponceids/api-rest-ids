package com.example.ids.MyAPIs.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.ids.MyAPIs.entities.Alumno;
import com.example.ids.MyAPIs.entities.Contacto;
import com.example.ids.MyAPIs.repositories.ContactoRepository;

@Service
public class ContactoServiceImpl implements ContactoService{
	
	@Autowired
	private ContactoRepository repository;
	@Override
	public List<Contacto> consultaContacto() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}

	@Override
	public Contacto consultaContacto(long contactoId) {
		// TODO Auto-generated method stub
		Optional<Contacto> contacto= repository.findById(contactoId);
		if(contacto.isPresent()) {
			return contacto.get();
		}
		return new Contacto();
	}

	@Override
	public void guardarContacto(Contacto contacto) {
		// TODO Auto-generated method stub
		repository.save(contacto);
		
	}

	@Override
	public void borrarContacto(long contactoId) {
		// TODO Auto-generated method stub
		repository.deleteById(contactoId);
		
	}

	@Override
	public void actualizarContacto(Contacto contacto) {
		// TODO Auto-generated method stub
		repository.save(contacto);
	}

}
