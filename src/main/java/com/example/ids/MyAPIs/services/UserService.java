package com.example.ids.MyAPIs.services;

import java.util.List;
import com.example.ids.MyAPIs.entities.User;

public interface UserService {
	List<User> consultaUsers();
	User consultaUser(long userId);
	void guardarUser(User user);
	void borrarUser(long userId);
	void actualizarUser(User user);
}
