package com.example.ids.MyAPIs.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.example.ids.MyAPIs.entities.Post;
import com.example.ids.MyAPIs.repositories.PostRepository;

@Service
public class PostServiceImpl implements PostService{
	@Autowired
	private PostRepository repository;
	@Override
	public List<Post> consultaPost() {
		return repository.findAll();
	}
	@Override
	public Post consultaPost(long postId) {
		Optional<Post> post = repository.findById(postId);
		if(post.isPresent()) {
			return post.get();
		}
		return new Post();
	}
	@Override
	public void guardarPost(Post post) {
		repository.save(post);
	}
	@Override
	public void borrarPost(long postId) {
		repository.deleteById(postId);
	}
	@Override
	public void actualizarPost(Post post) {
		repository.save(post);
	}
}
