package com.example.ids.MyAPIs.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.example.ids.MyAPIs.entities.User;
import com.example.ids.MyAPIs.repositories.UserRepository;

@Service
public class UserServiceImpl implements UserService{
	@Autowired
	private UserRepository repository;
	@Override
	public List<User> consultaUsers() {
		return repository.findAll();
	}
	@Override
	public User consultaUser(long userId) {
		Optional<User> user = repository.findById(userId);
		if(user.isPresent()) {
			return user.get();
		}
		return new User();
	}
	@Override
	public void guardarUser(User user) {
		repository.save(user);
	}
	@Override
	public void borrarUser(long userId) {
		repository.deleteById(userId);
	}
	@Override
	public void actualizarUser(User user) {
		repository.save(user);
	}
}
