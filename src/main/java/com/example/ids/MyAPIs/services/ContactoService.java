package com.example.ids.MyAPIs.services;

import java.util.List;

import com.example.ids.MyAPIs.entities.Contacto;

public interface ContactoService {
	List<Contacto> consultaContacto();
	Contacto consultaContacto(long contactoId);
	void guardarContacto(Contacto contacto);
	void borrarContacto(long contactoId);
	void actualizarContacto(Contacto contacto);
}
