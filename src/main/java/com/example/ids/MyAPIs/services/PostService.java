package com.example.ids.MyAPIs.services;

import java.util.List;
import com.example.ids.MyAPIs.entities.Post;

public interface PostService {
	List<Post> consultaPost();
	Post consultaPost(long postId);
	void guardarPost(Post post);
	void borrarPost(long postId);
	void actualizarPost(Post post);
}
