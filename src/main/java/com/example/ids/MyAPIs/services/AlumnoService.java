package com.example.ids.MyAPIs.services;

import java.util.List;
import com.example.ids.MyAPIs.entities.Alumno;

public interface AlumnoService {
	List<Alumno> consultaAlumno();
	Alumno consultaAlumno(long alumnoId);
	void guardarAlumno(Alumno alumno);
	void borrarAlumno(long alumnoId);
	void actualizarAlumno(Alumno alumno);
}
