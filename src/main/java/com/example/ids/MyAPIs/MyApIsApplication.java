package com.example.ids.MyAPIs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyApIsApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyApIsApplication.class, args);
	}

}
