package com.example.ids.MyAPIs.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.ids.MyAPIs.entities.Post;

public interface PostRepository extends JpaRepository<Post, Long>{

}
